Source: ocaml-portaudio
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Samuel Mimram <smimram@debian.org>, Romain Beauxis <toots@rastageeks.org>
Build-Depends:
 cdbs (>= 0.4.85~),
 debhelper (>= 10),
 dh-buildinfo,
 ocaml-nox,
 dh-ocaml (>= 0.9),
 portaudio19-dev,
 ocaml-findlib (>= 1.2.4),
 ocamlbuild,
 pkg-config
Standards-Version: 3.9.2
Homepage: https://github.com/savonet/ocaml-portaudio
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-portaudio.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-portaudio

Package: libportaudio-ocaml
Architecture: any
Depends: ${ocaml:Depends}, ${shlibs:Depends}, ${misc:Depends}
Provides: ${ocaml:Provides}
Description: OCaml bindings for the portaudio library
 Portaudio is a portable audio I/O library designed for cross-platform
 support of audio. This package provides OCaml interface to this library.
 .
 This package contains only the shared runtime stub libraries.

Package: libportaudio-ocaml-dev
Architecture: any
Depends: ${ocaml:Depends},
         portaudio19-dev, libportaudio-ocaml (= ${binary:Version}),
         ocaml-findlib, ${misc:Depends}
Provides: ${ocaml:Provides}
Description: OCaml bindings for the portaudio library
 Portaudio is a portable audio I/O library designed for cross-platform
 support of audio. This package provides OCaml interface to this library.
 .
 This package contains all the development stuff you need to develop
 OCaml programs which use portaudio.
